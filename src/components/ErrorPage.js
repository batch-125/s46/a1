import React from 'react';
import {
	Container,
	Row,
	Col,
	Nav,
	Jumbotron
} from 'react-bootstrap';

import {NavLink} from 'react-router-dom';

export default function Error(){

	return(
		<Container  fluid>
			<Row  className="text-center">
				<Col className="px-0">
					<Jumbotron fluid className="px-3">
						<h1>ERROR 404 - Link Not Found</h1>
						<p>The page you are looking for is not found</p>
						<Nav.Link as={NavLink} to="/">Go Back to Home Page</Nav.Link>
					</Jumbotron>
				</Col>
			</Row>
		</Container>
		
	)

}