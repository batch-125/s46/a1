import React from 'react';
import{
	Row,
	Col,
	Card,
	Button
} from 'react-bootstrap';
export default function Highlights(){

	return(

		<Row className="px-3">
			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Learn from Home</Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet consectetur adipisicing, elit. Consectetur perspiciatis ut, odio at cupiditate debitis, reiciendis aperiam accusamus ad, in voluptatum numquam quae laboriosam, sint. Hic recusandae consequuntur, eos pariatur!
				    </Card.Text>
				    <Button variant="primary">See more</Button>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Study Now, Pay Later</Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet consectetur adipisicing, elit. Consectetur perspiciatis ut, odio at cupiditate debitis, reiciendis aperiam accusamus ad, in voluptatum numquam quae laboriosam, sint. Hic recusandae consequuntur, eos pariatur!
				    </Card.Text>
				    <Button variant="primary">See more</Button>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Be Part of our Community</Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet consectetur adipisicing, elit. Consectetur perspiciatis ut, odio at cupiditate debitis, reiciendis aperiam accusamus ad, in voluptatum numquam quae laboriosam, sint. Hic recusandae consequuntur, eos pariatur!
				    </Card.Text>
				    <Button variant="primary">See more</Button>
				  </Card.Body>
				</Card>
			</Col>

		</Row>
	)
}