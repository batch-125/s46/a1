import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';//nakay react na talaaga to. validate if nakakareceive ng values

// react-bootstrap components
import {Card,Button} from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function Course({courseProp}){
	
	// console.log(courseProp)

	const {name, description, price, _id} = courseProp
	// console.log(name)

	// [state, setState/ = useState(initial value);
	// const [count, setCount] = useState(0);
	// const [seat, setSeat] = useState(10);
	// const [isDisabled, setIsDisabled] = useState(false);


	// function enroll(){
	// 	if(count < 10){
	// 		setCount( count + 1)
	// 		setSeat( seat - 1)
	// 	}
	// 	// } else {
	// 	// 	setIsDisabled(true)
	// 	// }
		
	// }

	// useEffect(() => {
	// 	if( seat === 0 ){
	// 		setIsDisabled(true)
	// 	}

	// }, [seat]);

	return (
		<Card className="mb-3 mx-5">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
					<h5>Description</h5>
					<p>{description}</p>
					<h5>Price:</h5>
					<p>{price}</p>
					{/*<h5>Enrollees</h5>
					<p>{count} Enrollees</p>*/}
					{/*<h5>Stocks</h5>*/}
					{/*<p>{seat} Seats</p>*/}
					{/*<Button variant="primary" */}

					 {/*onClick={*/}
					{/*enroll*/}
					 	{/*() => { setSeat( seat - 1)}*/}
						{/*// () => {*/}
					{/*	seat > 0 
						 	? (setCount( count + 1)
						 	  setSeat( seat - 1) 
						 	  )
							  
						 	: alert("No Seat Available")
						 }
					 }
					disabled={isDisabled}>Enroll*/}
					{/*</Button>*/}
					<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
			</Card.Body>
		</Card>
	)
}


Course.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

// const increment = () =>  
// setEnrolleeCount(enrolleeCount + 1);
// setSeatCount(seatCount - 1)
		
// enrolleeCount < 30 
// ? increment()
// : alert('maximum enrollees reached');
// reassign both statements to a single variable