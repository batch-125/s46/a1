import React, {useContext,useEffect, useState} from 'react';

import UserContext from './../UserContext';

import {
	Container,
	Button,
	Card
} from 'react-bootstrap';

import {Link, useParams} from 'react-router-dom';

export default function SpecificCourse(){

	const[name, setName] = useState('')
	const[description, setDescription] = useState('')
	const[price, setPrice] = useState(0)

	const {user} = useContext(UserContext);

	const {courseId} = useParams();
	let token = localStorage.getItem("token")
	useEffect( () => {
		
		fetch(`https://sashopee.herokuapp.com/api/products/${courseId}`,{
			method: "GET",
			headers: {
				"Authorization": ` Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result,"result ni single course")

// SET NA SA CONTAINER YUNG VALUE
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
		})
	}, [])

	// DITO LAGI magisismula magdefine ng function para sa mga buttons to click and submit

	const

	return(
		<Container>
			<Card>
				<Card.Header>
					<h4>{name}</h4>
				</Card.Header>
				<Card.Body>
					<Card.Text>
						{/*course descrtiption*/}
						{description}
					</Card.Text>
					<h6>
						price: PHP
						{/*course price*/}
						<span className="mx-2">{price}</span>
					</h6>
				</Card.Body>
				<Card.Footer>
					{
						(user.id !== null)
						? <Button variant="primary" onClick={(e) => enroll(e)}>Enroll</Button>
						: <Link className="btn btn-danger" to="">Login to Enroll</Link>	 
					}
				</Card.Footer>
			</Card>
		</Container>
	)
}