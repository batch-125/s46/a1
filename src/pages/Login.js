import React,{useState,useEffect, useContext} from 'react';
import {Redirect} from 'react-router-dom';

import {
	Form,
	Container,
	Button
} from 'react-bootstrap';

// Context
import UserContext from './../UserContext';

export default function Login(){

	const [email, setEmail] =useState('');
	const [password, setPassword] =useState('');
	// const [verifyPassword, setVerifyPassword] =useState('');
	const [isDisabled, setIsDisabled] =useState(true);

	// Destructure context object
	const {user, setUser} = useContext(UserContext);

	useEffect(() => {
		if( email !== '' && password !== ''){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)

		}

	}, [email, password] );

	function login(e){
		e.preventDefault();

		fetch('https://sashopee.herokuapp.com/api/users/login',{
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: email,
				    password: password
				})
		})
		.then(result => result.json())
		.then(result => {
			// console.log(result) // access: token

			if(typeof result.access !== "undefined"){
				//what should we do with the access token
				localStorage.setItem('token',result.access)
				userDetails(result.access)
				console.log("result.access", result.access)
			}
		})
		// let token = localStorage.getItem('token')
		const userDetails = (token) => { //pass token as parameter para bigyan value
			fetch('https://sashopee.herokuapp.com/api/users/details', {
				method: "GET",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(result => result.json())
			.then(result => {
				// console.log("userDetail sa login",result)
				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				});
			})
		}

		// alert(`Hi ${email}`);
 
		// update the user using email
		setUser({email: email});

		// save to local storage
		localStorage.setItem('email', email)

		setEmail('')
		setPassword('')
	}

	// check user value first ( context is an object)
	// console.log(user)

	// if(user.email !== null){
	// 	return <Redirect to='/' />
	// }

	return(
		(user.id !== null) 
		? <Redirect to="/"/>
		: 
			<Container className="mb-5">
				<h1 className="text-center">Login</h1>
				<Form onSubmit={(e) => login(e)}>
					<Form.Group className="mb-3" controlId="formBasicEmail">
						<Form.Label>Email address</Form.Label>
						<Form.Control type="email" placeholder="Enter email" value={email}onChange= {(e)=>setEmail(e.target.value)}/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="formBasicPassword">
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Password" value={password} onChange={(e)=> setPassword(e.target.value)}/>
					</Form.Group>

					<Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
				</Form>
			</Container>
	)
}