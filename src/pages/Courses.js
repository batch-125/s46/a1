import React, {useState, useEffect, useContext} from 'react';


// react-bootstrap components
import {Container} from 'react-bootstrap';
import AdminView from './../components/AdminView';
import UserView from './../components/UserView';
import UserContext from './../UserContext';
// components
import Course from './../components/Course'; //itong Course is how it will be displayed

// mock data
// import courses from './../mock-data/courses';

export default function Courses(){
	
	const [courses, setCourses] = useState([]);

	const {user} = useContext(UserContext)

	const fetchData = () => {
		const token = localStorage.getItem("token")
		fetch('https://sashopee.herokuapp.com/api/products/all', {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			// console.log(result)
			setCourses(result)
		})

	}


	useEffect( () => {
		fetchData()
	}, [])
	// fetch
	

	// display courses using .map()

	return (
		<Container className="p-4">
			{ 	(user.isAdmin === true)
				?	<AdminView courseData={courses} fetchData={fetchData}/>
				: 	<UserView courseData={courses}/>
			}	
		</Container>
	)


	
// 	let CourseCards = courses.map((course) =>{

// 		return <Course key={course.id} course={course}/>
// 		// key={course.id} purpose is
// 	}) 

// 	return(
// 		<Container fluid>
// 			{CourseCards}
// 		</Container>
// 	)
}