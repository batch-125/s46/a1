export default [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate eius consequatur quidem velit consequuntur culpa inventore alias enim possimus. Laudantium cum ipsum autem totam ea alias, voluptate possimus dolores. Dignissimos.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate eius consequatur quidem velit consequuntur culpa inventore alias enim possimus. Laudantium cum ipsum autem totam ea alias, voluptate possimus dolores. Dignissimos.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate eius consequatur quidem velit consequuntur culpa inventore alias enim possimus. Laudantium cum ipsum autem totam ea alias, voluptate possimus dolores. Dignissimos.",
		price: 55000,
		onOffer: true
	}

]