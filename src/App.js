import React, {useState, useEffect} from 'react';
import {
	BrowserRouter,
	Switch,
	Route,
} from 'react-router-dom';

// Context
import UserContext from './UserContext';

// react bootstrap components
import AppNavbar from "./components/AppNavbar";
import ErrorPage from "./components/ErrorPage";
// import CourseCard from "./components/CourseCard";
// import Welcome from './components/Welcome';
import Home from './pages/Home';
import Courses from './pages/Courses';
// import Counter from './components/Counter';
import Register from './pages/Register';
import Login from './pages/Login';
import SpecificCourse from './pages/SpecificCourse'
export default function App(){

	const [user, setUser] = useState({
		id: null,
		isAdmin: null
	});

	const unsetUser = () => {
		localStorage.clear();
		setUser({
			id: null,
			admin: null
		})
	}

	useEffect(() => {
		let token = localStorage.getItem("token")
		fetch('https://sashopee.herokuapp.com/api/users/details',{
			method: "GET",
			headers: {
				"Authorization": `bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result) //object // document of user

			if(typeof result !== "undefined"){
				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				})
				} else {
					setUser({
						id: null,
						isAdmin: null
					})
				}
			}
		)
	}, [])

	return (
		// <Fragment>
		//  	<AppNavbar/>
		//  	<Home/>
		// 	<CourseCard/>
		// 	<Welcome name="John"/>
		//   	<Welcome name="Aidan"/>
		//   	<Courses/>
		//  	<Counter/>
		//  	<Register/>
		//  	<Login/>
		// </Fragment>

	<UserContext.Provider value={{user, setUser, unsetUser}}>	
		<BrowserRouter>
			<AppNavbar />
			<Switch>
				<Route exact path="/" component={Home}/>
				<Route exact path="/courses" component={Courses}/>
				<Route exact path="/register" component={Register}/>
				<Route exact path="/login" component={Login}/>
				<Route exact path="/courses/:courseId" component={SpecificCourse}/>
				<Route component={ErrorPage}/>
			</Switch>
		</BrowserRouter>
	</UserContext.Provider>	

	)
};
